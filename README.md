Written by Ilya Malinovsky for https://jira.tower-research.com/browse/NPM-356
March 9th, 2018

## GENERAL
This utility sends UDP packet with the local time of the server. The idea is to timestamp and capture the packet on the wire
and then it will be possible to estimate the fidelity of the local timestamp compared to wire captures. 
For example, it will help to analyze discrepancy between timestamps in the GTO log vs PCTK
There is a couple of calibration mechanisms to reduce the jitter of the userspace in the code

It relates to NPM-356 JIRA incidet

## USAGE
Ideally, use onload or libvma to bypass kernel, so timestamps are more precise. 
Also, increase the priority and pin to an idle core
```
VMA_SELECT_POLL_OS_RATIO=0 VMA_INTERNAL_THREAD_AFFINITY=5 VMA_SPEC=latency LD_PRELOAD=/usr/local/ofed-tower-4.1/usr/lib64/libvma.so chrt -f 81 taskset -c 4 /spare/local/imalinovsky/ping_time/ping_time 206.200.242.78  12345

```
