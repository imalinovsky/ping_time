CC=gcc 
CFLAGS= -lrt -ldl

all: ping_time
program: ping_time.o
program.o: ping_time.c 

clean: 
	rm -f ping_time ping_time.o
