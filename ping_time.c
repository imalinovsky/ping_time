/* This utility sends UDP packet with the local time of the server. The idea is to capture the packet om the wire
 * and then it will be possible to estimate the fidelity of the local timestamp cpmpared to wire captures. 
 * For exaample, it will help to analyze discrepancy between timestamps in the log vs PCTK
 * It relates to NPM-356 JIRA incidet
 */


#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>
#include <emmintrin.h>
#include "pingpong.h"


/* The duration of the sleep is not material. I don't want to use non-deterministic pipelines with sleep, so just _mm_pause million times, whatever duration it will be */
static void pause_spinner() {
    uint64_t i;
    for (i=100000000;i>0;i--)
           _mm_pause();
}


int main(int argc, char** argv)
{
 
    long cycles_per_nsec;
    struct sockaddr_in addr;
    int fd;
    int nanoseconds_int;

    struct timespec ts;
    union tsc_t t1, t2;
    int conv_cost;

    const uint UTC = strlen ("2012-12-31 12:59:59" );
    const uint NANO = strlen ("123456789");

    char timestr [UTC + NANO + 1];
    char payload [UTC + NANO + 1];
    char up_to_seconds [UTC+1];
    char nanoseconds [NANO + 1];

    cycles_per_nsec = get_cycles_per_sec()/1E9;

    if ( argc != 3 ) /* First arg is address, second is port */
    {
        printf( "Usage: %s ip_address port\n", argv[0] );
        exit (1);
    }

    /* create a  UDP socket */
    if ((fd=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP)) < 0) {
        perror("socket");
        exit(1);
    }

    /* set up destination */
    memset(&addr,0,sizeof(addr));
    addr.sin_family=AF_INET;
    addr.sin_addr.s_addr=inet_addr(argv[1]);
    addr.sin_port=htons((unsigned short)strtoul(argv[2], NULL, 0));
     
    /* now just sendto(). The destination doesn't have to be reachable, but the packet should end up in the capture somehow */
    while (1) {

        clock_gettime(CLOCK_REALTIME, &ts);

        __my_rdtsc_pre__(t1); //Start measuring cost of measuring time

        timespec2str(timestr, sizeof(timestr), &ts); //Convert time into string that can be then used as a messagew in the payload

        strncpy(up_to_seconds, timestr, UTC ); //get UTC portion without partial second
        up_to_seconds [UTC] ='\0';

        strncpy(nanoseconds, timestr + UTC + 1 , 10); //get nanoseconds. +1 here is to skip the dot
        nanoseconds_int = atoi (nanoseconds);

        __my_rdtsc_post__(t2); //Stop measuring the cost of measuring time

        conv_cost = (int)(t2.v-t1.v)/cycles_per_nsec;

        if (nanoseconds_int > conv_cost)  { // For brievity, don't want to deal with rollovers, just skip values that will roll over
  
            sprintf (payload, "%s.%09d", up_to_seconds, nanoseconds_int + conv_cost); //format the payload
     
            if (sendto(fd, payload, sizeof(payload), 0, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
                perror("sendto");
                exit(1);
     
           }
    
           printf("%s, Cost of converting and sending time %d nsec, payload sent %s\n", timestr,conv_cost,payload) ;
    
       }

        pause_spinner ();
    }
}
